/**
 * Christopher Tran / CSAP
 * User: christopher
 * Date: 11/17/13
 * Time: 4:20 PM
 * Host: tranit-rmworkstation.ad.it.chtr.us
 * <p/>
 * Private Data:
 * Static:
 * Methods:
 */
public class Senior extends Student {
    private boolean commSrv;
    private double fees;
    public Senior(String ifirst, String ilast, int igradeLevel, double iqpa, char icommunityservice, double ifees) {
        super(ifirst, ilast, igradeLevel, iqpa);
        commSrv = icommunityservice=='y';
        fees = ifees;
    }

    public boolean isCommSrv() {
        return commSrv;
    }

    public double getFees() {
        return fees;
    }

    public String isCommSrvStr() {
        if(commSrv)
            return "has";
        else
            return "has not";
    }

    @Override
    public String toString() {
        return getLast() + ", " + getFirst() + " in grade " + getGradeLevel() + " has a QPA of " + getQpa() +
                ", " + isCommSrvStr() + " completed community service and owes $" + fees + " in fees.";
    }
}
