/**
 * Christopher Tran / CSAP
 * User: christopher
 * Date: 11/12/13
 * Time: 9:06 AM
 * Host: tranit-rmworkstation.ad.it.chtr.us
 * <p/>
 * Private Data:
 * Static:
 * Methods:
 */
import java.io.*;
import java.util.*;
public class AllTheStudents {
    private ArrayList<Student> studentArray = new ArrayList<Student>();
    private String filename;
    public AllTheStudents() {}

    public void sortArray() {
        Student temp;
        int count = studentArray.size();
        if(studentArray.size()>0) {
            boolean swap = true;
            while(swap) {
                swap = false;
                count--;
                for(int x=0;x<count;x++) {
                    if(studentArray.get(x).compareTo(studentArray.get(x + 1)) > 0) {
                        temp = studentArray.get(x);
                        studentArray.set(x, studentArray.get(x+1));
                        studentArray.set(x+1, temp);
                        swap = true;
                    }
                }
            }
        }
    }

    private String inputMethod(String prompt) throws IOException {
        InputStreamReader reader = new InputStreamReader(System.in);
        BufferedReader input = new BufferedReader(reader);

        System.out.print(prompt + ": ");
        return input.readLine();
    }

    public void setFileName() throws IOException {
        do {
            filename = inputMethod("Enter the filename");

            if (filename.equals("") || filename == null || !checkIfExists()) {
                System.out.println("File not found or insufficient permissions. Try again.");
            }
        }
        while (filename.equals("") || filename == null || !checkIfExists());
    }

    public void readFile() throws IOException {
        String inputString;

        FileReader reader = new FileReader(filename);
        BufferedReader inFile = new BufferedReader(reader);

        inputString = inFile.readLine();
        while(inputString != null) {
            StringTokenizer st = new StringTokenizer(inputString);
            while(st.hasMoreTokens()) {
                String fn = st.nextToken(),
                        ln = st.nextToken();
                Integer gr = Integer.parseInt(st.nextToken());
                Double  gp = Double.parseDouble(st.nextToken());

                switch(gr) {
                    case 9:
                        studentArray.add(new Freshman(fn,ln,gr,gp,Integer.parseInt(st.nextToken())));
                        break;
                    case 10:
                        studentArray.add(new Sophomore(fn,ln,gr,gp,Integer.parseInt(st.nextToken())));
                        break;
                    case 11:
                        studentArray.add(new Junior(fn,ln,gr,gp,st.nextToken()));
                        break;
                    case 12:
                        studentArray.add(new Senior(fn,ln,gr,gp,st.nextToken().charAt(0),Double.parseDouble(st.nextToken())));
                        break;
                }
            }
            inputString = inFile.readLine();
        }
    }

    private boolean checkIfExists() {
        File check = new File(filename);
        return check.isFile();
    }

    public void getStudents() {
        for(Student var : studentArray)
            System.out.println(var.toString());
    }
    public static void main(String args[]) throws IOException {
        AllTheStudents derp = new AllTheStudents();

        derp.setFileName();
        derp.readFile();
        derp.sortArray();
        derp.getStudents();
    }
}
