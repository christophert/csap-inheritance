/**
 * Christopher Tran / CSAP
 * User: christopher
 * Date: 11/12/13
 * Time: 8:08 AM
 * Host: tranit-rmworkstation.ad.it.chtr.us
 * <p/>
 * Private Data:
 * Static:
 * Methods:
 */
import java.util.*;
import java.io.*;
public class Student {

    private String first, last;
    private int gradeLevel;
    private double qpa;
    public Student(String ifirst, String ilast, int igradeLevel, double iqpa) {
        first = ifirst;
        last = ilast;
        gradeLevel = igradeLevel;
        qpa = iqpa;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public int getGradeLevel() {
        return gradeLevel;
    }

    public void setGradeLevel(int gradeLevel) {
        this.gradeLevel = gradeLevel;
    }

    public double getQpa() {
        return qpa;
    }

    public void setQpa(double qpa) {
        this.qpa = qpa;
    }

    public int compareTo(Student g) {
        if(g.last.equals(last)) {
            return first.compareToIgnoreCase(g.first);
        }
        return last.compareToIgnoreCase(g.last);
    }

    @Override
    public String toString() {
        return first + " " + last + " in grade " + gradeLevel + " has a QPA of " + qpa;
    }

}
