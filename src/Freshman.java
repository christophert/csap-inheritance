/**
 * Christopher Tran / CSAP
 * User: christopher
 * Date: 11/17/13
 * Time: 4:19 PM
 * Host: tranit-rmworkstation.ad.it.chtr.us
 * <p/>
 * Private Data:
 * Static:
 * Methods:
 */
public class Freshman extends Student {
    int dr;
    public Freshman(String ifirst, String ilast, int igradeLevel, double iqpa, int idr) {
        super(ifirst, ilast, igradeLevel, iqpa);
        dr = idr;
    }

    public int getDr() {
        return dr;
    }

    @Override
    public String toString() {
        return getLast() + ", " + getFirst() + " in grade " + getGradeLevel() + " has a QPA of " + getQpa() +
                " and had " + dr + " discipline referrals at DMS.";
    }
}
