/**
 * Christopher Tran / CSAP
 * User: christopher
 * Date: 11/17/13
 * Time: 4:20 PM
 * Host: tranit-rmworkstation.ad.it.chtr.us
 * <p/>
 * Private Data:
 * Static:
 * Methods:
 */
public class Junior extends Student {
    private String pssa;
    public Junior(String ifirst, String ilast, int igradeLevel, double iqpa, String ipssa) {
        super(ifirst, ilast, igradeLevel, iqpa);
        if(ipssa.equals("belowbasic"))
            pssa = "Below Basic";
        else if (ipssa.equals("basic"))
            pssa = "Basic";
        else if(ipssa.equals("proficient"))
            pssa = "Proficient";
        else
            pssa = "Advanced";
    }

    public String getPssa() {
        return pssa;
    }

    @Override
    public String toString() {
        return getLast() + ", " + getFirst() + " in grade " + getGradeLevel() + " has a QPA of " + getQpa() +
                " and got " + pssa + " on the PSSA.";
    }
}
