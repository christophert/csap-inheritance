/**
 * Christopher Tran / CSAP
 * User: christopher
 * Date: 11/17/13
 * Time: 4:19 PM
 * Host: tranit-rmworkstation.ad.it.chtr.us
 * <p/>
 * Private Data:
 * Static:
 * Methods:
 */
public class Sophomore extends Student {
    private int psat;
    public Sophomore(String ifirst, String ilast, int igradeLevel, double iqpa, int ipsat) {
        super(ifirst, ilast, igradeLevel, iqpa);
        psat = ipsat;
    }

    public int getPsat() {
        return psat;
    }

    @Override
    public String toString() {
        return getLast() + ", " + getFirst() + " in grade " + getGradeLevel() + " has a QPA of " + getQpa() +
                " and got " + psat + " on the PSAT.";
    }
}
